/*
    Copyright (C) 2014, 2020  Christoph Gärtner

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

let table = (() => {
    let cell = document.createElement('td');

    let row = document.createElement('tr');
    for(let i = 0; i < 8; ++i)
        row.appendChild(cell.cloneNode(true));
    row.appendChild(document.createElement('th'));

    let table = document.createElement('table');
    for(let i = 0; i < 8; ++i) {
        let clone = table.appendChild(row.cloneNode(true));
        clone.lastChild.appendChild(document.createTextNode(8 - i));
    }

    for(let i = 0; i < 64; ++i) {
        let cell = table.childNodes[i / 8 | 0].childNodes[i % 8];
        cell.title = cell.id = String.fromCharCode(
            'a'.charCodeAt(0) + (i % 8),
            '8'.charCodeAt(0) - (i / 8 | 0));
    }

    let labels = table.appendChild(document.createElement('tr'));
    for(let i = 0; i < 8; ++i) {
        labels.appendChild(
            document.createElement('th').appendChild(
                document.createTextNode('abcdefgh'.charAt(i))).parentNode);
    }

    return table;
})();

let selection = null;

function dup(obj, mixin) {
    return Object.assign({}, obj, mixin);
}

function cellAt(pos) {
    return table.rows[pos / 8 | 0].cells[pos % 8];
}

function render(state) {
    switch(state.color) {
        case 1: document.body.className = 'white'; break;
        case 2: document.body.className = 'black'; break;
    }

    for(let pos = 0; pos < 64; ++pos) {
        let cell = cellAt(pos);
        cell.innerHTML = symbol(state.board.charAt(pos));
        cell.className = selection === null
            ? 'selectable'
            : 'unselectable';
    }

    if(selection !== null) {
        let cell = table.rows[selection / 8 | 0].cells[selection % 8];
        cell.className = 'selected';
        if(cell.chessMoves) for(let pos of cell.chessMoves)
            cellAt(pos).className = 'targetted';
    }
    else if(history.state.promo) {
        selection = history.state.promo;
        cellAt(selection).className = 'promoting';
    }
    else setTimeout(() => {
        for(let pos = 0; pos < 64; ++pos) {
            let x = pos % 8;
            let y = pos / 8 | 0;
            let piece = history.state.board.charAt(pos);

            let cell = table.rows[y].cells[x];
            let moves = cell.chessMoves = reach(state, pos);
            if(moves.length > 0)
                cell.className = 'movable';
        }
    }, 0);
}

function next(state, serial) {
    history.pushState(state, document.title,
        '?' + (serial === undefined ? serialize(state) : serial));

    selection = null;
    render(state);
}

function nextDup(mixin) {
    next(dup(history.state, mixin));
}

function clearBoard() {
    next(deserialize(EMPTY), EMPTY);
}

function resetBoard() {
    next(deserialize(NEW), '');
}

function switchTurn() {
    nextDup({ color: other(history.state.color) });
}

function spawnPiece(piece) {
    if(selection === null)
        return;

    let state = history.state;
    let cell = state.board.charAt(selection);

    if(empty(cell)) {
        let y = (selection / 8 | 0);
        if(any(piece, 'Pp') &&  (y === 0 || y === 7))
            return;

        nextDup({ board: place(state.board, selection, piece) });
    }
    else if(selection === state.promo) {
        switch(state.color) {
            case 1:
            if(any(piece, 'QRBN')) {
                nextDup({
                    board: place(state.board, selection, piece),
                    color: 2,
                    promo: undefined,
                });
            }
            break;

            case 2:
            if(any(piece, 'qrbn')) {
                nextDup({
                    board: place(state.board, selection, piece),
                    color: 1,
                    promo:undefined,
                });
            }
            break;
        }
    }
}

function removeSelected() {
    if(selection === null)
        return;

    let state = history.state;
    nextDup({
        board: place(state.board, selection, '_'),
        promo: selection === state.promo ? undefined : state.promo,
    });
}

function queryCastlingState() {
    var input = prompt('Castling state:', 'KQkq');
    if(input === null)
        return;

    let castling = {};
    for(i = 0; i < input.length; ++i)
        castling[input.charAt(i)] = true;

    nextDup({ castling: castling })
}

function setEnPassantTarget() {
    nextDup({ ep: selection !== null ? selection : undefined });
}

function flip(value) {
    document.documentElement.className = value ? 'flipping' : '';
}

onpopstate = () => {
    selection = null;
    render(history.state);
};

onclick = (event) => {
    let target = event.target;
    switch(target.nodeName.toUpperCase()) {
        case 'INPUT':
        switch(target.id) {
            case 'K':
            case 'Q':
            case 'R':
            case 'B':
            case 'N':
            case 'P':
            case 'k':
            case 'q':
            case 'r':
            case 'b':
            case 'n':
            case 'p':
            spawnPiece(target.id);
            break;

            case 'reset': resetBoard(); break;
            case 'clear': clearBoard(); break;
            case 'switch': switchTurn(); break;
            case 'remove': removeSelected(); break;
            case 'castling': queryCastlingState(); break;
            case 'en-passant': setEnPassantTarget(); break;

            case 'flipper': flip(target.checked); break;
        }
        break;

        case 'TD':
        switch(target.className) {
            case 'selectable':
            case 'movable':
            selection = posof(event.target.id);
            render(history.state);
            break;

            case 'selected':
            selection = null;
            render(history.state);
            break;

            case 'targetted':
            let src = selection;
            let dest = posof(event.target.id);

            let state = play(history.state, src, dest);
            next(state);
            break;
        }
        break;
    }
};

addEventListener('DOMContentLoaded', () => {
    document.getElementById('board').replaceWith(table);
    let state = deserialize(location.search.substring(1) || NEW);
    history.replaceState(state, document.title, location.search || '?');
    render(state);
});

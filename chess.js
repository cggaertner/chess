/*
    Copyright (C) 2020  Christoph Gärtner

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

let NEW = 'rnbqkbnr8p32_8PRNBQKBNR-w-KQkq--';
let EMPTY = '64_-w---';

function any(val, set) {
    return set.indexOf(val) >= 0;
}

function other(color) {
    return 3 - color;
}

function nameof(pos) {
    return pos === undefined ? ''
        : String.fromCharCode(
            'a'.charCodeAt(0) + (pos % 8),
            '8'.charCodeAt(0) - (pos / 8 | 0));
}

function posof(name) {
    return name.length === 0 ? undefined
        : name.charCodeAt(0) - 'a'.charCodeAt(0)
            + 8 * ('8'.charCodeAt(0) - name.charCodeAt(1));
}

function expand(board) {
    return board.replace(/(\d+)(\D)/g, (m, p1, p2) => p2.repeat(+p1));
}

function collapse(board) {
    return board.replace(/(\w)\1+/g, (m, p1) => String(m.length) + p1);
}

function serialize(state) {
    let color;
    switch(state.color) {
        case 1: color = 'w'; break;
        case 2: color = 'b'; break;
    }

    return [
        collapse(state.board),
        color,
        Object.keys(state.castling).join(''),
        nameof(state.ep),
        nameof(state.promo),
    ].join('-');
}

function deserialize(string) {
    let tokens = string.split('-');

    let color;
    switch(tokens[1]) {
        case 'w': color = 1; break;
        case 'b': color = 2; break;
    }

    let castling = {};
    for(i = 0; i < tokens[2].length; ++i)
        castling[tokens[2].charAt(i)] = true;

    return {
        board: expand(tokens[0]),
        color: color,
        castling: castling,
        ep: posof(tokens[3]),
        promo: posof(tokens[4]),
    };
}

function symbol(piece) {
    switch(piece) {
        case 'K': return '♔';
        case 'Q': return '♕';
        case 'R': return '♖';
        case 'B': return '♗';
        case 'N': return '♘';
        case 'P': return '♙';
        case 'k': return '♚';
        case 'q': return '♛';
        case 'r': return '♜';
        case 'b': return '♝';
        case 'n': return '♞';
        case 'p': return '♟';
        case '_': return '\xA0';
    }
}

function fromSymbol(sym) {
    switch(sym) {
        case '♔': return 'K';
        case '♕': return 'Q';
        case '♖': return 'R';
        case '♗': return 'B';
        case '♘': return 'N';
        case '♙': return 'P';
        case '♚': return 'k';
        case '♛': return 'q';
        case '♜': return 'r';
        case '♝': return 'b';
        case '♞': return 'n';
        case '♟': return 'p';
        case '\xA0': return '_';
    }
}

function friend(color, piece) {
    switch(color) {
        case 1:
        switch(piece) {
            case 'K': return true;
            case 'Q': return true;
            case 'R': return true;
            case 'B': return true;
            case 'N': return true;
            case 'P': return true;
            default : return false;
        }
        break;

        case 2:
        switch(piece) {
            case 'k': return true;
            case 'q': return true;
            case 'r': return true;
            case 'b': return true;
            case 'n': return true;
            case 'p': return true;
            default : return false;
        }
        break;
    }
}

function enemy(color, piece) {
    switch(color) {
        case 1:
        switch(piece) {
            case 'k': return true;
            case 'q': return true;
            case 'r': return true;
            case 'b': return true;
            case 'n': return true;
            case 'p': return true;
            default : return false;
        }
        break;

        case 2:
        switch(piece) {
            case 'K': return true;
            case 'Q': return true;
            case 'R': return true;
            case 'B': return true;
            case 'N': return true;
            case 'P': return true;
            default : return false;
        }
        break;
    }
}

function empty(piece) {
    return piece === '_';
}

function play(state, src, dest) {
    state = Object.assign({}, state);

    let piece = state.board.charAt(src);

    // perform move
    state.board = move(state.board, src, dest);

    // don't change player if pawn needs to be promoted
    if(state.color === 1 && piece === 'P' && (dest / 8 | 0) === 0)
        state.promo = dest;
    else if(state.color === 2 && piece === 'p' && (dest / 8 | 0) === 7)
        state.promo = dest;
    else {
        state.promo = undefined;
        state.color = 3 - state.color;
    }

    // castling
    if(piece === 'K' && src === 60) {
        if(dest === 62)
            state.board = move(state.board, 63, 61);
        else if(dest === 58)
            state.board = move(state.board, 56, 59);
    }
    else if(piece === 'k' && src === 4) {
        if(dest === 6)
            state.board = move(state.board, 7, 5);
        else if(dest === 2)
            state.board = move(state.board, 0, 3);
    }

    // en passant
    if(piece === 'P' && dest === state.ep)
        state.board = place(state.board, dest + 8, '_');
    else if(piece === 'p' && dest === state.ep)
        state.board = place(state.board, dest - 8, '_');

    // set en passant target
    if(piece === 'P' && dest - src === -16) state.ep = src - 8;
    else if(piece === 'p' && dest - src === 16) state.ep = src + 8;
    else state.ep = undefined;

    // update castling state
    switch(piece) {
        case 'K':
        delete state.castling.Q;
        delete state.castling.K;
        break;

        case 'R':
        switch(src) {
            case 56: delete state.castling.Q; break;
            case 63: delete state.castling.K; break;
        }
        break;

        case 'k':
        delete state.castling.q;
        delete state.castling.k;
        break;

        case 'r':
        switch(src) {
            case 0: delete state.castling.q; break;
            case 7: delete state.castling.k; break;
        }
        break;
    }

    return state;
}

function place(board, pos, piece) {
    return board.substring(0, pos) + piece + board.substring(pos + 1);
}

function move(board, src, dest) {
    let fields = board.split('');
    fields[dest] = fields[src];
    fields[src] = '_';
    return fields.join('');
}

function kingof(color) {
    switch(color) {
        case 1: return 'K';
        case 2: return 'k';
    }
}

function reach(state, src) {
    let at = (x, y) => state.board.charAt(y * 8 + x);
    let to = (x, y) => y * 8 + x;

    let moves = [];
    let piece = state.board.charAt(src);
    if(piece === '_')
        return moves;

    // determine potential moves

    let x0 = src % 8
    let y0 = src / 8 | 0;
    switch(state.color) {
        case 1:
        switch(piece) {
            case 'K':
            if(y0 > 0) {
                if(x0 > 0 && !friend(1, at(x0 - 1, y0 - 1)))
                    moves.push(to(x0 - 1, y0 - 1));
                if(!friend(1, at(x0, y0 - 1)))
                    moves.push(to(x0, y0 - 1));
                if(x0 < 7 && !friend(1, at(x0 + 1, y0 - 1)))
                    moves.push(to(x0 + 1, y0 - 1));
            }
            {
                if(x0 > 0 && !friend(1, at(x0 - 1, y0)))
                    moves.push(to(x0 - 1, y0));
                if(x0 < 7 && !friend(1, at(x0 + 1, y0)))
                    moves.push(to(x0 + 1, y0));
            }
            if(y0 < 7) {
                if(x0 > 0 && !friend(1, at(x0 - 1, y0 + 1)))
                    moves.push(to(x0 - 1, y0 + 1));
                if(!friend(1, at(x0, y0 + 1)))
                    moves.push(to(x0, y0 + 1));
                if(x0 < 7 && !friend(1, at(x0 + 1, y0 + 1)))
                    moves.push(to(x0 + 1, y0 + 1));
            }
            if(src === 60) {
                if(state.castling.Q
                        && empty(at(1, 7))
                        && empty(at(2, 7))
                        && empty(at(3, 7))
                        && !threatened(1, state.board, 58)
                        && !threatened(1, state.board, 59)
                        && !threatened(1, state.board, 60))
                    moves.push(58);
                if(state.castling.K
                        && empty(at(5, 7))
                        && empty(at(6, 7))
                        && !threatened(1, state.board, 60)
                        && !threatened(1, state.board, 61)
                        && !threatened(1, state.board, 62))
                    moves.push(62);
            }
            break;

            case 'Q':
            for(let x = x0, y = y0; x > 0;) {
                --x;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7;) {
                ++x;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y > 0;) {
                --y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y < 7;) {
                ++y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x > 0 && y > 0;) {
                --x, --y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y < 7;) {
                ++x, ++y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x > 0 && y < 7;) {
                --x, ++y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y > 0;) {
                ++x, --y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            break;

            case 'R':
            for(let x = x0, y = y0; x > 0;) {
                --x;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7;) {
                ++x;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y > 0;) {
                --y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y < 7;) {
                ++y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            break;

            case 'B':
            for(let x = x0, y = y0; x > 0 && y > 0;) {
                --x, --y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y < 7;) {
                ++x, ++y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x > 0 && y < 7;) {
                --x, ++y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y > 0;) {
                ++x, --y;
                let target = at(x, y);
                if(!friend(1, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            break;

            case 'N':
            if(x0 > 1 && y0 > 0 && !friend(1, at(x0 - 2, y0 - 1)))
                moves.push(to(x0 - 2, y0 - 1));
            if(x0 > 0 && y0 > 1 && !friend(1, at(x0 - 1, y0 - 2)))
                moves.push(to(x0 - 1, y0 - 2));
            if(x0 < 7 && y0 > 1 && !friend(1, at(x0 + 1, y0 - 2)))
                moves.push(to(x0 + 1, y0 - 2));
            if(x0 < 6 && y0 > 0 && !friend(1, at(x0 + 2, y0 - 1)))
                moves.push(to(x0 + 2, y0 - 1));
            if(x0 > 1 && y0 < 7 && !friend(1, at(x0 - 2, y0 + 1)))
                moves.push(to(x0 - 2, y0 + 1));
            if(x0 > 0 && y0 < 6 && !friend(1, at(x0 - 1, y0 + 2)))
                moves.push(to(x0 - 1, y0 + 2));
            if(x0 < 7 && y0 < 6 && !friend(1, at(x0 + 1, y0 + 2)))
                moves.push(to(x0 + 1, y0 + 2));
            if(x0 < 6 && y0 < 7 && !friend(1, at(x0 + 2, y0 + 1)))
                moves.push(to(x0 + 2, y0 + 1));
            break;

            case 'P':
            if(y0 === 6 && empty(at(x0, y0 - 1)) && empty(at(x0, y0 - 2)))
                moves.push(to(x0, y0 - 2));
            if(y0 > 0 && empty(at(x0, y0 - 1)))
                moves.push(to(x0, y0 - 1));
            if(y0 > 0 && x0 > 0 && (enemy(1, at(x0 - 1, y0 - 1))
                    || to(x0 - 1, y0 - 1) === state.ep))
                moves.push(to(x0 - 1, y0 - 1));
            if(y0 > 0 && x0 < 7 && (enemy(1, at(x0 + 1, y0 - 1))
                    || to(x0 + 1, y0 - 1) === state.ep))
                moves.push(to(x0 + 1, y0 - 1));
            break;
        }
        break;

        case 2:
        switch(piece) {
            case 'k':
            if(y0 > 0) {
                if(x0 > 0 && !friend(2, at(x0 - 1, y0 - 1)))
                    moves.push(to(x0 - 1, y0 - 1));
                if(!friend(2, at(x0, y0 - 1)))
                    moves.push(to(x0, y0 - 1));
                if(x0 < 7 && !friend(2, at(x0 + 1, y0 - 1)))
                    moves.push(to(x0 + 1, y0 - 1));
            }
            {
                if(x0 > 0 && !friend(2, at(x0 - 1, y0)))
                    moves.push(to(x0 - 1, y0));
                if(x0 < 7 && !friend(2, at(x0 + 1, y0)))
                    moves.push(to(x0 + 1, y0));
            }
            if(y0 < 7) {
                if(x0 > 0 && !friend(2, at(x0 - 1, y0 + 1)))
                    moves.push(to(x0 - 1, y0 + 1));
                if(!friend(2, at(x0, y0 + 1)))
                    moves.push(to(x0, y0 + 1));
                if(x0 < 7 && !friend(2, at(x0 + 1, y0 + 1)))
                    moves.push(to(x0 + 1, y0 + 1));
            }
            if(src === 4) {
                if(state.castling.q
                        && empty(at(1, 0))
                        && empty(at(2, 0))
                        && empty(at(3, 0))
                        && !threatened(2, state.board, 2)
                        && !threatened(2, state.board, 3)
                        && !threatened(2, state.board, 4))
                    moves.push(2);
                if(state.castling.k
                        && empty(at(5, 0))
                        && empty(at(6, 0))
                        && !threatened(2, state.board, 4)
                        && !threatened(2, state.board, 5)
                        && !threatened(2, state.board, 6))
                    moves.push(6);
            }
            break;

            case 'q':
            for(let x = x0, y = y0; x > 0;) {
                --x;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7;) {
                ++x;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y > 0;) {
                --y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y < 7;) {
                ++y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x > 0 && y > 0;) {
                --x, --y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y < 7;) {
                ++x, ++y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x > 0 && y < 7;) {
                --x, ++y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y > 0;) {
                ++x, --y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            break;

            case 'r':
            for(let x = x0, y = y0; x > 0;) {
                --x;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7;) {
                ++x;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y > 0;) {
                --y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; y < 7;) {
                ++y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            break;

            case 'b':
            for(let x = x0, y = y0; x > 0 && y > 0;) {
                --x, --y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y < 7;) {
                ++x, ++y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x > 0 && y < 7;) {
                --x, ++y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            for(let x = x0, y = y0; x < 7 && y > 0;) {
                ++x, --y;
                let target = at(x, y);
                if(!friend(2, target)) moves.push(to(x, y));
                if(!empty(target)) break;
            }
            break;

            case 'n':
            if(x0 > 1 && y0 > 0 && !friend(2, at(x0 - 2, y0 - 1)))
                moves.push(to(x0 - 2, y0 - 1));
            if(x0 > 0 && y0 > 1 && !friend(2, at(x0 - 1, y0 - 2)))
                moves.push(to(x0 - 1, y0 - 2));
            if(x0 < 7 && y0 > 1 && !friend(2, at(x0 + 1, y0 - 2)))
                moves.push(to(x0 + 1, y0 - 2));
            if(x0 < 6 && y0 > 0 && !friend(2, at(x0 + 2, y0 - 1)))
                moves.push(to(x0 + 2, y0 - 1));
            if(x0 > 1 && y0 < 7 && !friend(2, at(x0 - 2, y0 + 1)))
                moves.push(to(x0 - 2, y0 + 1));
            if(x0 > 0 && y0 < 6 && !friend(2, at(x0 - 1, y0 + 2)))
                moves.push(to(x0 - 1, y0 + 2));
            if(x0 < 7 && y0 < 6 && !friend(2, at(x0 + 1, y0 + 2)))
                moves.push(to(x0 + 1, y0 + 2));
            if(x0 < 6 && y0 < 7 && !friend(2, at(x0 + 2, y0 + 1)))
                moves.push(to(x0 + 2, y0 + 1));
            break;

            case 'p':
            if(y0 === 1 && empty(at(x0, y0 + 1)) && empty(at(x0, y0 + 2)))
                moves.push(to(x0, y0 + 2));
            if(y0 < 7 && empty(at(x0, y0 + 1)))
                moves.push(to(x0, y0 + 1));
            if(y0 < 7 && x0 > 0 && (enemy(2, at(x0 - 1, y0 + 1))
                    || to(x0 - 1, y0 + 1) === state.ep))
                moves.push(to(x0 - 1, y0 + 1));
            if(y0 < 7 && x0 < 7 && (enemy(2, at(x0 + 1, y0 + 1))
                    || to(x0 + 1, y0 + 1) === state.ep))
                moves.push(to(x0 + 1, y0 + 1));
            break;
        }
        break;
    }

    // remove illegal moves

    let king = kingof(state.color);
    let kingpos = state.board.indexOf(king);
    if(kingpos < 0)
        return moves;

    let legalMoves = [];

    NEXT: for(let dest of moves) {
        let board = move(state.board, src, dest);
        let pos = piece === king ? dest : kingpos;

        if(!threatened(state.color, board, pos))
            legalMoves.push(dest);
    }

    return legalMoves;
}

function threatened(color, board, pos) {
    let at = (x, y) => board.charAt(y * 8 + x);

    let x0 = pos % 8;
    let y0 = pos / 8 | 0;

    switch(color) {
        case 1:
        // look for bishops and queens in diagonal directions
        for(let x = x0, y = y0; x > 0 && y > 0;) {
            --x, --y;
            if(any(at(x, y), 'bq')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x < 7 && y < 7;) {
            ++x, ++y;
            if(any(at(x, y), 'bq')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x > 0 && y < 7;) {
            --x, ++y;
            if(any(at(x, y), 'bq')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x < 7 && y > 0;) {
            ++x, --y;
            if(any(at(x, y), 'bq')) return true;
            if(at(x, y) !== '_') break;
        }

        // look for rooks and queens in x/y directions
        for(let x = x0, y = y0; x > 0;) {
            --x;
            if(any(at(x, y), 'rq')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x < 7;) {
            ++x;
            if(any(at(x, y), 'rq')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; y > 0;) {
            --y;
            if(any(at(x, y), 'rq')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; y < 7;) {
            ++y;
            if(any(at(x, y), 'rq')) return true;
            if(at(x, y) !== '_') break;
        }

        // look for knights
        if(x0 > 1 && y0 > 0 && at(x0 - 2, y0 - 1) === 'n') return true;
        if(x0 > 0 && y0 > 1 && at(x0 - 1, y0 - 2) === 'n') return true;
        if(x0 < 7 && y0 > 1 && at(x0 + 1, y0 - 2) === 'n') return true;
        if(x0 < 6 && y0 > 0 && at(x0 + 2, y0 - 1) === 'n') return true;
        if(x0 > 1 && y0 < 7 && at(x0 - 2, y0 + 1) === 'n') return true;
        if(x0 > 0 && y0 < 6 && at(x0 - 1, y0 + 2) === 'n') return true;
        if(x0 < 7 && y0 < 6 && at(x0 + 1, y0 + 2) === 'n') return true;
        if(x0 < 6 && y0 < 7 && at(x0 + 2, y0 + 1) === 'n') return true;

        // look for pawns
        if(y0 > 0 && x0 > 0 && at(x0 - 1, y0 - 1) === 'p') return true;
        if(y0 > 0 && x0 < 7 && at(x0 + 1, y0 - 1) === 'p') return true;

        // look for kings
        if(y0 > 0) {
            if(x0 > 0 && at(x0 - 1, y0 - 1) === 'k') return true;
            if(x0 < 7 && at(x0 + 1, y0 - 1) === 'k') return true;
            if(          at(x0    , y0 - 1) === 'k') return true;
        }
        {
            if(x0 > 0 && at(x0 - 1, y0    ) === 'k') return true;
            if(x0 < 7 && at(x0 + 1, y0    ) === 'k') return true;
        }
        if(y0 < 7) {
            if(x0 > 0 && at(x0 - 1, y0 + 1) === 'k') return true;
            if(          at(x0    , y0 + 1) === 'k') return true;
            if(x0 < 7 && at(x0 + 1, y0 + 1) === 'k') return true;
        }
        break;

        case 2:
        // look for bishops and queens in diagonal directions
        for(let x = x0, y = y0; x > 0 && y > 0;) {
            --x, --y;
            if(any(at(x, y), 'BQ')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x < 7 && y < 7;) {
            ++x, ++y;
            if(any(at(x, y), 'BQ')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x > 0 && y < 7;) {
            --x, ++y;
            if(any(at(x, y), 'BQ')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x < 7 && y > 0;) {
            ++x, --y;
            if(any(at(x, y), 'BQ')) return true;
            if(at(x, y) !== '_') break;
        }

        // look for rooks and queens in x/y directions
        for(let x = x0, y = y0; x > 0;) {
            --x;
            if(any(at(x, y), 'RQ')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; x < 7;) {
            ++x;
            if(any(at(x, y), 'RQ')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; y > 0;) {
            --y;
            if(any(at(x, y), 'RQ')) return true;
            if(at(x, y) !== '_') break;
        }
        for(let x = x0, y = y0; y < 7;) {
            ++y;
            if(any(at(x, y), 'RQ')) return true;
            if(at(x, y) !== '_') break;
        }

        // look for knights
        if(x0 > 1 && y0 > 0 && at(x0 - 2, y0 - 1) === 'N') return true;
        if(x0 > 0 && y0 > 1 && at(x0 - 1, y0 - 2) === 'N') return true;
        if(x0 < 7 && y0 > 1 && at(x0 + 1, y0 - 2) === 'N') return true;
        if(x0 < 6 && y0 > 0 && at(x0 + 2, y0 - 1) === 'N') return true;
        if(x0 > 1 && y0 < 7 && at(x0 - 2, y0 + 1) === 'N') return true;
        if(x0 > 0 && y0 < 6 && at(x0 - 1, y0 + 2) === 'N') return true;
        if(x0 < 7 && y0 < 6 && at(x0 + 1, y0 + 2) === 'N') return true;
        if(x0 < 6 && y0 < 7 && at(x0 + 2, y0 + 1) === 'N') return true;

        // look for pawns
        if(y0 < 7 && x0 > 0 && at(x0 - 1, y0 + 1) === 'P') return true;
        if(y0 < 7 && x0 < 7 && at(x0 + 1, y0 + 1) === 'P') return true;

        // look for kings
        if(y0 > 0) {
            if(x0 > 0 && at(x0 - 1, y0 - 1) === 'K') return true;
            if(x0 < 7 && at(x0 + 1, y0 - 1) === 'K') return true;
            if(          at(x0    , y0 - 1) === 'K') return true;
        }
        {
            if(x0 > 0 && at(x0 - 1, y0    ) === 'K') return true;
            if(x0 < 7 && at(x0 + 1, y0    ) === 'K') return true;
        }
        if(y0 < 7) {
            if(x0 > 0 && at(x0 - 1, y0 + 1) === 'K') return true;
            if(          at(x0    , y0 + 1) === 'K') return true;
            if(x0 < 7 && at(x0 + 1, y0 + 1) === 'K') return true;
        }
        break;
    }

    return false;
}
